require('./bootstrap');
import Vue from 'vue/dist/vue'

window.Vue = Vue
// window.Vue = require('vue');

// import dependecies tambahan
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import Axios from 'axios';
import Vuex from 'vuex';

Vue.use(VueRouter,VueAxios,Axios,Vuex);

// import file yang dibuat tadi
import Home from './views/Home.vue'

import Ujian from './views/exams/Index.vue'
import Tugas from './views/tasks/Index.vue'
import Allresult from './views/results/By-type.vue'

//for login component
import Store from './Store.js'
import Login from './views/auth/Login.vue'

// membuat router
const routes = [
    {
        path: '/murid-v2/login',
        name: 'login',
        component: Login,
        meta: {
            requiresAuth: false,
            layout: 'guest-layout'
        },
    },
    {
        path :'/',
        name: 'home',
        component: Home,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/murid-v2/ujian', 
        name : 'ujian', 
        component: Ujian,
        meta: {
            requiresAuth: true,
            layout: 'user-layout'
        },
    },
    {
        path: '/murid-v2/tugas', 
        name : 'tugas', 
        component: Tugas,
        meta: {
            requiresAuth: true,
            layout: 'user-layout'
        },
    },
    {
        path: '/murid-v2/result-all', 
        name : 'all-result', 
        component: Allresult,
        meta: {
            requiresAuth: true,
            layout: 'user-layout'
        },
    },
    {
        path: '/murid-v2/result-by-type/:id', 
        name : 'result-by', 
        component: Tugas,
        meta: {
            requiresAuth: true,
            layout: 'admin-layout'
        },
    }
]

const router = new VueRouter({ mode: 'history', routes: routes });
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let auth = Store.getters.isAuth
        if (!auth) {
            next({ name: 'login' })
        } else {
            next()
        }
    } else {
        next()
    }
})


export default router