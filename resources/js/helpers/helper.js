import axios from 'axios';
import Vue from 'vue';
import VueFlashMessage from 'vue-flash-message';
import 'vue-flash-message/dist/vue-flash-message.min.css';

Vue.use(VueFlashMessage, {
  messageOptions: {
    timeout: 3000,
    pauseOnInteract: true
  }
});

const vm = new Vue();

const base_url = window.location.origin+'/api/v1';

const dashboard_url = base_url+'/dashboard';
const schoolInfo = base_url+'/school-info';
const notifications_url = base_url+'/notifications';
const leaderboard_url = base_url+'/leaderboard';
const response_url = base_url+'/teacher-response';
const schedule_url = base_url+'/schedule';
const schedule_v2_url = base_url+'/schedule_v2';
const nilai_url = base_url+'/result-by-type';

const handleError = fn => (...params) =>
  fn(...params).catch(error => {
    if (error.response.status == 401) {
      localStorage.clear();
    }
    vm.flash(`${error.response.status}: ${error.response.statusText}`, 'error');
  });

const headers =  {
        Authorization: localStorage.getItem('token') != 'null' ? 'Bearer ' + localStorage.getItem('token'):'',
        'Content-Type': 'application/json'
}

export const api = {
    //dashboard
      dashboard: handleError(async function(offset='', limit=''){
        const res = await axios.get(dashboard_url+'?offset='+offset+'&limit='+limit,{
          headers: headers
        })
        return res.data;
    }),
    //school info
    school: handleError(async function(){
      const res = await axios.get(schoolInfo,{
        headers: headers
      })
      return res.data;
    }),
    //notifications
    notifications: handleError(async function(){
      const res = await axios.get(notifications_url, {
        headers: headers
      })
      return res.data;
    }),
    //leaderbord
    leaderboard:handleError(async function(offset='', limit=''){
      const res = await axios.get(leaderboard_url+'?offset='+offset+'&limit='+limit,{
        headers: headers
      })
      return res.data;
    }),
    // response
    response:handleError(async function(offset='', limit=''){
      const res = await axios.get(response_url+'?offset='+offset+'&limit='+limit,{
        headers: headers
      })
      return res.data;
    }),

    //schedule
    schedule:handleError(async function(type=1, offset='',limit='', date=''){
      const res = await axios.get(schedule_url+'?type='+type+'&offset='+offset+'&limit='+limit, {
        headers:headers
      });
      return res.data;
    }), 
    //schedule
    scheduleV2:handleError(async function(type=1, offset='',limit='', date=''){
      const res = await axios.get(schedule_v2_url+'?type='+type+'&offset='+offset+'&limit='+limit+'&date='+date, {
        headers:headers
      });
      return res.data;
    })

//     //user
//   getuser: handleError(async id => {
//     const res = await axios.get(userURL+'/'+ id);
//     return res.data;
//   }),
//   getusersaktif: handleError(async() => {
//     const res = await axios.get(userURLaktif);
//     return res.data;
//   }),
//   getusersinactive:handleError(async() => {
//     const res = await axios.get(userGetNonaktif);
//     return res.data;
//   }),
//   getusersinactivepaginate:handleError(async id =>{
//     const res = await axios.get('https://bakulan.web.id/api/getusersinactive?page='+ id);
//     return res.data;
//   }),

//   getusers: handleError(async () => {
//     const res = await axios.get(userURL);
//     return res.data;
//   }),
//   deleteusers: handleError(async id => {
//     const res = await axios.delete(userURL +'/'+ id);
//     return res.data;
//   }),
//   createuser: handleError(async payload => {
//     const res = await axios.post(userURL, payload);
//     return res.data;
//   }),
//   updateuser: handleError(async payload => {
//     const res = await axios.put(userURL +'/'+payload.id, payload);
//     return res.data;
//   }),
//   aktifornotuser:handleError(async id => {
//     const res = await axios.post(userNonAktif+'/'+ id);
//     return res.data;
//   }),
//   getusersPaginate:handleError(async id =>{
//     const res = await axios.get('https://bakulan.web.id/api/penjual?page='+ id);
//     return res.data;
//   }),

//   //kota kabupaten
//   getkota: handleError(async id => {
//     const res = await axios.get(kabupatenURL+'/'+ id);
//     return res.data;
//   }),
//   getkotas: handleError(async () => {
//     const res = await axios.get(kabupatenURL);
//     return res.data;
//   }),
//   deletekotas: handleError(async id => {
//     const res = await axios.delete(kabupatenURL +'/'+ id);
//     return res.data;
//   }),
//   createkota: handleError(async payload => {
//     const res = await axios.post(kabupatenURL, payload);
//     return res.data;
//   }),
//   updatekota: handleError(async payload => {
//     const res = await axios.put(kabupatenURL +'/'+payload.id, payload);
//     return res.data;
//   }),

//   getKotaByProvinsi:handleError(async id => {
//     const res = await axios.get(provinsiURLget+'/'+ id);
//     return res.data;
//   }),
//   getkecamatansByRegionId:handleError(async id => {
//     const res = await axios.get(kabupatenURLget+'/'+ id);
//     return res.data;
//   }),

//   //kecamatan
//   getkecamatan: handleError(async id => {
//     const res = await axios.get(kecamatanURL+'/'+ id);
//     return res.data;
//   }),
//   getkecamatans: handleError(async () => {
//     const res = await axios.get(kecamatanURL);
//     return res.data;
//   }),
//   deletekecamatans: handleError(async id => {
//     const res = await axios.delete(kecamatanURL +'/'+ id);
//     return res.data;
//   }),
//   createkecamatan: handleError(async payload => {
//     const res = await axios.post(kecamatanURL, payload);
//     return res.data;
//   }),
//   updatekecamatan: handleError(async payload => {
//     const res = await axios.put(kecamatanURL +'/'+payload.id, payload);
//     return res.data;
//   }),

//   getPasarByKecamatanId:handleError(async id => {
//     const res = await axios.get(kecamatanURLget+'/'+ id);
//     return res.data;
//   }),

//   //provinsi
//   getprovinsies:handleError(async()=>{
//     const res = await axios.get(provinsiURL);
//     return res.data;
//   }),
//   getprovinsi: handleError(async id => {
//     const res = await axios.get(provinsiURL+'/'+ id);
//     return res.data;
//   }),
//   createprovinsi: handleError(async payload => {
//     const res = await axios.post(provinsiURL, payload);
//     return res.data;
//   }),
//   updateprovinsi: handleError(async payload => {
//     const res = await axios.put(provinsiURL +'/'+payload.id, payload);
//     return res.data;
//   }),

//   deleteprovinsi: handleError(async id => {
//     const res = await axios.delete(provinsiURL +'/'+ id);
//     return res.data;
//   }),
//   //pasar
//   getpasars: handleError(async () => {
//     const res = await axios.get(pasarURL);
//     return res.data;
//   }),
//   getpasar: handleError(async id => {
//     const res = await axios.get(pasarURL+'/'+ id);
//     return res.data;
//   }),

//   deletepasars: handleError(async id => {
//     const res = await axios.delete(pasarURL +'/'+ id);
//     return res.data;
//   }),
//   createpasar: handleError(async payload => {
//     const res = await axios.post(pasarURL, payload);
//     return res.data;
//   }),
//   updatepasar: handleError(async payload => {
//     const res = await axios.put(pasarURL +'/'+payload.id, payload);
//     return res.data;
//   }),


//   //get list pasar by kota
//   getPasarByCity: async function(region_id){
//     console.log(region_id)
//     const res = await axios.get(listPasarByUrl+'?region_id='+region_id)
//     return res.data;
// },
//   //get list user/penjual by kota and pasar
//   getPenjualBy: async function(kode_provinsi, kode_region,kode_pasar,lainnya){
//       const res = await axios.get(getPenjualBy+'?kode_provinsi='+kode_provinsi+'&kode_pasar='+kode_pasar+'&kode_region='+kode_region+'&lainnya='+lainnya)
//       return res.data;
//   }
//   getPasarByCity: handleError(async region_id => {
//     const res = await axios.get(listPasarByUrl, region_id);
//     return res.data;
//   }),
};

const $axios = axios.create({
    baseURL: base_url,
    headers: {
        Authorization: localStorage.getItem('token') != 'null' ? 'Bearer ' + JSON.stringify(localStorage.getItem('token')):'',
        'Content-Type': 'application/json'
    }
});

export default $axios;