import Vue from 'vue'
import router from './router.js'
import store from './Store.js'
import App from './layouts/App.vue';


new Vue(Vue.util.extend({ router,store }, App,store)).$mount("#app");