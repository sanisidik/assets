import $axios from '../helpers/helper'

const state = () => ({

})

const mutations = {

}

const actions = {
    submit({ commit }, payload) {
        localStorage.setItem('token', null) //RESET LOCAL STORAGE MENJADI NULL
        commit('SET_TOKEN', null, { root: true }) //RESET STATE TOKEN MENJADI NULL
        //KARENA MUTATIONS SET_TOKEN BERADA PADA ROOT STORES, MAKA DITAMBAHKAN PARAMETER
        //{ root: true }

        //KITA MENGGUNAKAN PROMISE AGAR FUNGSI SELANJUTNYA BERJALAN KETIKA FUNGSI INI SELESAI
        return new Promise((resolve, reject) => {
            //MENGIRIM REQUEST KE SERVER DENGAN URI /login
            //DAN PAYLOAD ADALAH DATA YANG DIKIRIMKAN DARI COMPONENT LOGIN.VUE
            $axios.post('/login', payload)
            .then((response) => {
                //KEMUDIAN JIKA RESPONNYA SUKSES
                if (response.data.status == 'success') {
                    //MAKA LOCAL STORAGE DAN STATE TOKEN AKAN DISET MENGGUNAKAN
                    //API DARI SERVER RESPONSE
                    //handle if not students 
                    if(response.data.is_college == "0"){
                        commit('SET_ERRORS', { invalid: 'Anda tidak terdaftar sebagai siswa Silahkan Hubungi Admin' }, { root: true })
                    }else{
                        const token = response.data.token
                        const user_id = response.data.user.id
                        const user_name = response.data.user.name
                        localStorage.setItem('token', token)
                        localStorage.setItem('user_id', user_id)
                        localStorage.setItem('user_name', user_id)
                        commit('SET_TOKEN', token, { root: true })
                    }
                } else {
                    commit('SET_ERRORS', { invalid: 'Email/Password Salah' }, { root: true })
                }
                //JANGAN LUPA UNTUK MELAKUKAN RESOLVE AGAR DIANGGAP SELESAI
                resolve(response.data)
            })
            .catch((error) => {
                console.log(error)
                if (error.response.status == 401) {
                    commit('SET_ERRORS', error.response.data.errors, { root: true })
                }
            })

        })
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}